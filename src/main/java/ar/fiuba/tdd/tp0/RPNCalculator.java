package ar.fiuba.tdd.tp0;

import java.util.Stack;

public class RPNCalculator {

    Stack<Expression> calculator;

    public float eval(String expression) {
        if(expression == null) throw new IllegalArgumentException("Argument can't be null");
        calculator = new Stack<>();
        this.parseExpression(expression);
        return Float.valueOf(calculator.peek().expression);
    }

    private void parseExpression(String expression) {
        String[] parts = expression.split(" ");
        for (int i = 0; i < parts.length; i++) {
            Expression ex = new Expression(parts[i]);
            ex.evaluate(calculator, ex.getEvaluator());
        }
    }
}
