package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Stack;
import java.util.function.BooleanSupplier;


public class Expression {

    public String expression;
    boolean isOperator;
    boolean isNumber;
    boolean isNotValid;

    private static final HashMap<String, Calculator.IntegerMath> operators;
    private static final HashMap<Boolean, Evaluation> evaluators;

    static {
        operators = new HashMap<>();
        operators.put("/", Calculator.division);
        operators.put("//", Calculator.dobleDivision);
        operators.put("*", Calculator.multiplicacion);
        operators.put("**", Calculator.dobleMultiplicacion);
        operators.put("-", Calculator.resta);
        operators.put("--", Calculator.dobleResta);
        operators.put("+", Calculator.suma);
        operators.put("++", Calculator.dobleSuma);
        operators.put("MOD", Calculator.mod);
        evaluators = new HashMap<>();
    }

    interface Evaluation {
        void evaluating(Stack<Expression> calculator);
    }

    public void evaluate(Stack<Expression> calculator, Evaluation eval) {
        eval.evaluating(calculator);
    }

    public Expression(String express) {
        expression = express;
        isOperator = validationOperator.getAsBoolean();
        isNumber = validationNumber.getAsBoolean();
        isNotValid = !(isOperator || isNumber);
        evaluators.put(isOperator, evalOperator);
        evaluators.put(isNumber, evalNumber);
        evaluators.put(isNotValid, evalNotValid);
    }

    public Evaluation getEvaluator() {
        return evaluators.get(true);
    }

    BooleanSupplier validationOperator = () -> operators.get(expression) != null;

    BooleanSupplier validationNumber = () -> expression.matches("-?\\d+(\\.\\d+)?");

    Evaluation evalNumber = (Stack<Expression> calculator) -> {
        calculator.push(this);
    };

    Evaluation evalOperator = (Stack<Expression> calculator) -> {
        Calculator.IntegerMath operator = operators.get(expression);
        float resultado = operator.operation(calculator);
        calculator.push(new Expression(String.valueOf(resultado)));
    };

    Evaluation evalNotValid = (Stack<Expression> calculator) -> throwException();

    private void throwException() {
        throw new IllegalArgumentException("Not a valid expression");
    }
}
