package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Stack;
import java.util.Vector;
import java.util.function.Consumer;
import java.util.function.DoubleBinaryOperator;

public class Calculator {

    interface IntegerMath {
        float operation(Stack<Expression> calculator);
    }

    private static Vector<Float> getOperands(Stack<Expression> calculator, int qty, boolean isDoubleOperator) {
        Vector<Float> operands = new Vector<>();
        validateOperations(calculator, qty, isDoubleOperator);
        for (int i = 0; i < qty; i++) {
            String val = calculator.pop().expression;
            operands.add(Float.valueOf(val));
        }
        return operands;

    }

    private static void validateOperations(Stack<Expression> calculator, int qty, boolean isDoubleOperator) {
        HashMap<Boolean, Consumer> validator = new HashMap<>();
        Consumer invalid = (x) -> throwInvalidOperand();
        Consumer valid = (x) -> doNothing();
        validator.put(true, invalid);
        validator.put(false, valid);
        Consumer toDo = validator.get((calculator.size() < qty && !isDoubleOperator) || calculator.isEmpty());
        toDo.accept(null);
    }

    private static void throwInvalidOperand() {
        throw new IllegalArgumentException("Operator has not enough operands");
    }

    private static void doNothing() { }

    private static DoubleBinaryOperator sumar = (op1, op2) -> op1 + op2;
    private static DoubleBinaryOperator substraction = (op1, op2) -> op1 - op2;
    private static DoubleBinaryOperator multiply = (op1, op2) -> op1 * op2;
    private static DoubleBinaryOperator divide = (op1, op2) -> op1 / op2;
    private static DoubleBinaryOperator module = (op1, op2) -> op1 % op2;

    private static float applySimpleOperator(Stack<Expression> calculator, DoubleBinaryOperator simpleOperator) {
        Vector<Float> operands = getOperands(calculator, 2, false);
        return (float)simpleOperator.applyAsDouble(operands.elementAt(1), operands.elementAt(0));
    }

    private static float applyDoubleOperator(Stack<Expression> calculator, DoubleBinaryOperator simpleOperator) {
        float result = getOperands(calculator, 1, true).elementAt(0);
        while(!calculator.isEmpty()) {
            result = (float)simpleOperator.applyAsDouble(result, Float.valueOf(calculator.pop().expression));
        }
        return result;
    }

    static IntegerMath suma = (Stack<Expression> calculator) -> applySimpleOperator(calculator, sumar);

    static IntegerMath dobleSuma = (Stack<Expression> calculator) -> applyDoubleOperator(calculator, sumar);

    static IntegerMath resta = (Stack<Expression> calculator) -> applySimpleOperator(calculator, substraction);

    static IntegerMath dobleResta = (Stack<Expression> calculator) -> applyDoubleOperator(calculator, substraction);

    static IntegerMath division = (Stack<Expression> calculator) -> applySimpleOperator(calculator, divide);

    static IntegerMath dobleDivision = (Stack<Expression> calculator) -> applyDoubleOperator(calculator, divide);

    static IntegerMath multiplicacion = (Stack<Expression> calculator) -> applySimpleOperator(calculator, multiply);

    static IntegerMath dobleMultiplicacion = (Stack<Expression> calculator) -> applyDoubleOperator(calculator, multiply);

    static IntegerMath mod = (Stack<Expression> calculator) -> applySimpleOperator(calculator, module);
}
